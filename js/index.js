$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 10000
    });
    $("#contactoBtn").on("shown.bs.modal", function (e) {
        console.log('El modal se esta mostrando');
    });
});